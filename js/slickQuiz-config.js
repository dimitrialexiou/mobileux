// Setup your quiz text and questions here

// NOTE: pay attention to commas, IE struggles with those bad boys

var quizJSON = {
    "info": {
        "name":    "Ancient Greece Quiz",
        "main":    "<p>Let's see how smart you are!</p>",
        "results": "<h5>In order to learn more you should visit the following page</h5><p></p>",
        "level1":  "Master",
        "level2":  "Contender",
        "level3":  "Amateur",
        "level4":  "Newbie",
        "level5":  "Stay in school!" 
    },
    "questions": [
        { // Question 1 - Multiple Choice, Single True Answer
            "q": "Where the Temple of Apollo Epikourios were builded?",
            "a": [
                {"option": "Athens",      "correct": false},
                {"option": "Thiva",     "correct": false},
                {"option": "Bassae",      "correct": true},
 
            ],
            "correct": "<p><span>That's right!</span> The Temple of Apollo Epikourios ('Apollo the Helper') was built high on a rocky ridge of Mt. Kotylion at Bassae in south-west Arcadia, a region of the Greek Peloponnese. </p>",
            "incorrect": "<p><span>Noo!</span> You have to study harder!</p>" // no comma here
        },
        { // Question 2 - Multiple Choice, Multiple True Answers, Select Any
            "q": "Which of the following is represented on the silver medallion?",
            "a": [
                {"option": "Dionysus",               "correct": false},
                {"option": "Apollo",   "correct": false},

                {"option": "Aphrodite", "correct": true} 
            ],
            "select_any": true,
            "correct": "<p><span>Nice!</span> This silver medallion may have originally ornamented a bowl. It shows the goddess Aphrodite taking something offered to her by a girl attendant. </p>",
            "incorrect": "<p><span>Uhh no!!</span> You have to study harder!</p>" // no comma here
        },
        { // Question 3 - Multiple Choice, Multiple True Answers, Select All
            "q": "How many metres was the stadium that Hercules is said to have set the length? ",
            "a": [
                {"option": "192 metres",           "correct": true},
                {"option": "200 metres",                  "correct": false},

                {"option": "424 metres",          "correct": false} // no comma here
            ],
            "correct": "<p><span>Brilliant!</span> You're seriously a genius! Hercules is said to have set the length of the stadium at just over 192 metres, the distance he could run in one breath. During the Roman period guilds of professional athletes were formed, adopting Hercules as their patron.</p>",
            "incorrect": "<p><span>Mistake!!.</span> You have to study harder!</p>" // no comma here
        },
        { // Question 4
            "q": "From when to when Alexander the Great, reigned?",
            "a": [
                {"option": "340-367 BC",    "correct": false},
                {"option": "336-323 BC",     "correct": true},
                {"option": "345-367 BC",      "correct": false},

            ],
            "correct": "<p><span>Correct!</span> Literary sources tell us, though perhaps not reliably, that Alexander (reigned 336-323 BC) chose only a few artists to produce his image, and famous names such as the sculptor Lysippos and the painter Apelles were associated with his portraiture.</p>",
            "incorrect": "<p><span>False</span> You have to study harder!</p>" // no comma here
        },
        { // Question 5
            "q": "What does the sculpture from the famous temple on the Acropolis in Athens show?",
            "a": [
                 {"option": "A mythological battle",    "correct": true},
                {"option": "Butterflies",     "correct": false} 
 
            ],
            "correct": "<p><span>Great!</span> This sculpture from the famous temple on the Acropolis in Athens shows a mythological battle between a human Lapith and a barbaric centaur.</p>",
            "incorrect": "<p><span>No way!</span> You have to study harder!</p>" // no comma here
        },
         { // Question 6
            "q": "Who is the Greek God of Wine?",
            "a": [
                {"option": "Zeus",    "correct": false},
                {"option": "Dionysus",     "correct": true},
                {"option": "Aphrodite",      "correct": false},
 
            ],
            "correct": "<p><span>Bravo!</span> This sculpture shows Dionysus, Greek god of wine, and imitates Greek works in bronze made at the beginning of the fifth century BC. </p>",
            "incorrect": "<p><span>Too bad!</span> You have to study harder!</p>" // no comma here
        },
         { // Question 7
            "q": "What does the Parthenon frieze shows??",
            "a": [
                {"option": "Birds",    "correct": false},
                {"option": "Racing chariots",     "correct": true},
                {"option": "A battle",      "correct": false},
               
            ],
            "correct": "<p><span>Great!</span> The Parthenon frieze shows racing chariots ahead of the cavalcade of mounted horsemen. </p>",
            "incorrect": "<p><span>No way!</span> You have to study harder!</p>" // no comma here
        },
         { // Question 8
            "q": "What the plaques were made from?",
            "a": [
                {"option": "Marble",    "correct": false},
                {"option": "Sheet gold",     "correct": true},
                {"option": "Wood",      "correct": false},
        
            ],
            "correct": "<p><span>Great!</span> The plaques are of sheet gold, and are identical in form, though the added decoration in filigree and granulation on the dress of the goddess and the bodies of the lions is different in each case, creating a rich effect.</p>",
            "incorrect": "<p><span>No way!</span> You have to study harder!</p>" // no comma here
        },
         { // Question 9
            "q": "When did Cleopatra was born?",
            "a": [
                {"option": "69-68 BC",    "correct": true},
                {"option": "67-65 BC",     "correct": false} 
            ],
            "correct": "<p><span>Great!</span> Cleopatra was born in 69-68 BC. When Ptolemy XII Auletes (the 'flute-player') died in 51 BC, the 18-year-old Cleopatra and her brother Ptolemy XIII, aged ten, were named as his successors.</p>",
            "incorrect": "<p><span>No way!</span> You have to study harder!</p>" // no comma here
        },
         { // Question 10
            "q": "When did Socrates was living?",
            "a": [
                {"option": "469-399 BC",    "correct": true},
                {"option": "459-389 BC",     "correct": false} 
            ],
            "correct": "<p><span>Great!</span>Socrates (469-399 BC) is considered to be the intellectual father of modern Western philosophy. </p>",
            "incorrect": "<p><span>No way!</span> You have to study harder!</p>" // no comma here
        },
    
    ]
};
